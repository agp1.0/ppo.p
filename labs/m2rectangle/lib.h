#ifndef _LIB_H 
#define _LIB_H 
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define  BUFSZ 1024
#define  SEPARATOR " \t"

extern char   buf[];

                                      
typedef unsigned char  uchar;
typedef unsigned short ushort;
typedef unsigned int   uint;
typedef unsigned long  ulong;



extern                              //  ������� ���������� ��������� ���.������
int keycmp( const char * arg        //  � ������  �������� �� ��������
           , const char * key       //  ���� ����� ���������� � ������� -  ��� /
    );                              //  � ���������� key - � / ������ �� ����.


extern
char * strRplc(  char * oldstr      //  ������ ������ ������
               , const char * str   //  �� �����
       );

extern 
int isInt(const char *str);

extern 
int isDouble(const char *str) ;


#endif
