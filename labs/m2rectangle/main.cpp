#ifdef _DEBUG
#include <crtdbg.h>
//#define _CRTDBG_MAP_ALLOC
#endif

#include <time.h>
#include "lib.h"
#include "coor.h"

                

//     ���������� �� ����������� �����������
void help(const char * nm){
  char appNm[100] = {0};
  char buf[200] = {0};
  strcpy(buf, nm);
  char *tmp = strrchr (buf, '\\'); 
  if (tmp)
     strcpy(appNm, tmp+1);
  else 
     strcpy(appNm,nm);
  
  printf("application to build table of words\n");
  printf("usage:\n");
  printf("%s  [-?] [-v] [-d] [-8] \nwhere\n", appNm);
  printf("-?	this page\n");
  printf("-d	to debug\n");
  printf("-v	to show additional info\n");
  printf("-8    to print 8 digits after ., opposite - 6 only\n");
  exit(1);      // ��� ������ ���������� �� 0
}


int main(int argc, char *argv[]) {

#ifdef _DEBUG
//  ��������� ������� �������� �� �������� ������
     _CrtSetDbgFlag(
                 _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG)
              | _CRTDBG_CHECK_ALWAYS_DF 
              | _CRTDBG_LEAK_CHECK_DF
        );

//  ��� ���� ���������
//  �������� � ���� ������������ ������
/*

     _CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);
     _CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDERR);
     _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDERR);
*/
#endif




  int rc = 1;
  int only6     = 1; // ����� ������� ��� ����������
  int vFlag     = 0 ; // �������� ����� ������
  int i = 1;
  time_t   st = time(0);
  for (;i< argc; i++){
     if(keycmp(argv[i], "-?") )
        help (argv[0]);
     else if (keycmp(argv[i], "v") )
        vFlag = 1;
     else if (keycmp(argv[i], "d") )
        dFlag = 1;
     else if (keycmp(argv[i], "8") )
        only6 = 0;
  }

  rc = 0;

  if (dFlag)
     malloc(10);

  if (vFlag)
     fprintf(stderr, " length of digits/debug: %d/%d \n"
              , only6,  dFlag);

  coor2  test (1.0, 2.0);              // ��������� ������ �����������, ��� ������� ������
  coor2 *test2  = new coor2();         // ��������� ������ ����������� + ������ ������
  delete test2; test2=0;
  int  lineno = 0;
  char  *endLn=0;                           // ��������� �� ������ ����� ������
  char   *str=0;
  rectangle r ;                             // ��� �������� ����������� �� ���������
  coor2 * c = 0;
  char * endL=0;

  while (gets(buf)){                        //  
    lineno++;
    endL= (char *)memchr(buf, '#', BUFSZ); // ����� � ������� ����������
    if (endL)
       *endL=0;
    c = new coor2(buf) ;             // �������� ������ ��� ������ ���������
                                     //  ��� ������� � ������������ ������
    switch (c->wellFormed)   {
       case OK:
           r.expande (*c);
           break;
       case EMPTY:
           break;
       case WRONG:
           fprintf (stderr, "\n lineNo %d: error while reading this line: '%s' ", lineno, buf);
    }
    if (dFlag==0)
      delete c;                     //
  }
  printf ("\n rectangle: %s", r.print(only6));
  r.buf[0] = 0;  // just to show another way to access to static fields
  (&r)->buf[0] = 0;
  r.f();
  rectangle *r1 = &r;
  r1->f();
  rectangle::f();
  if (vFlag)
     fprintf(stderr, "\n %d secs of work!\n",  time(0) - st);

  return rc;                    // ��� �������� ����� 0 ��� 1
}

