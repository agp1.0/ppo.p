#include "lib.h"
#include "coor.h"


  int dFlag     = 0 ; // �������



  void coor2::ini (coor2 * this1, double x, double y, code_t wellFormed ){
     if (dFlag)
       fprintf (stderr, "\nini () is here");
     if (this1) {
        this1->x = x;
        this1->y = y;
        this1->wellFormed =  wellFormed;
     }
  }


  coor2::~coor2(){
     if (dFlag)
       fprintf (stderr, "\ndestructor coor2 () is here");
  }

  coor2::coor2(){
    if (dFlag)
       fprintf (stderr, "\ncoor2 () is here");
    ini(this, 0.0, 0.0, EMPTY);
  }

  coor2::coor2(double x, double y){
    if (dFlag)
      fprintf (stderr, "\ncoor2 (double x, double y) is here");
    ini(this, x, y, OK);
  }
  coor2::coor2( const char * str1){
    double x=0.0, y=0.0;
    code_t wellFormed = EMPTY;
    if (dFlag)
      fprintf (stderr, "\ncoor2(const char * str) is here: '%s'", str1);
    if (str1 && strlen(str1)> 0){
      char * str = strRplc(0, str1);
      char * xs  = strtok  (str, SEPARATOR);
      if (xs)   {
        wellFormed = WRONG;
        if (dFlag)
          fprintf (stderr, "\ncoor2(const char * str) is here: xs: '%s'", xs);
        if (isDouble(xs)){
           x = atof (xs);
           char * ys  = strtok  (0, SEPARATOR);
           if (ys)
              if (isDouble(ys)){
                 y = atof(ys);
                 wellFormed = OK;
                 if (dFlag)
                   fprintf (stderr, "\n coor2(const char * str): x/y: %f (%s)/%f(%s)", x, xs, y, ys);
              }
        }
      }
      free(str); str=0;
    }
    ini(this, x, y, wellFormed);
  }

// first way to access to static fields
 char rectangle::buf[BUFSZ]={0};


  rectangle::rectangle(){
    if (dFlag)
      fprintf (stderr, "\nrectangle() is here");
  }



  char*  rectangle::print(int only6){
    if (leftTop.wellFormed && rightBottom.wellFormed) {
       if (only6)
          sprintf (buf, "\nleft top: %.6f longitude   %.6f latitude  \nright bottom:  %.6f longitude  %.6f latitude "
                 , leftTop.y, leftTop.x    
                  ,  rightBottom.y, rightBottom.x);
       else
          sprintf (buf, "\nleft top: %.8f longitude   %.8f latitude  \nright bottom:  %.8f longitude  %.8f latitude "
                 , leftTop.y, leftTop.x    
                  ,  rightBottom.y, rightBottom.x);
    }
    else 
      sprintf (buf, "rectangle is not well formed");
    return buf;
  }

  void rectangle::f(){
    buf[0]= 0;
  }  
  void rectangle::expande(coor2 & p){
                 // ��� ������� ����� ������������ .
                 // � ������������� ���������� ������
                 // �� ���� ��������� �� 0
    if (p.wellFormed==OK) {
      if (this->leftTop.wellFormed ==OK && rightBottom.wellFormed == OK) {
         if  (p.x >  leftTop.x )
           leftTop.x = p.x;
         if  (p.x <  rightBottom.x )
           rightBottom.x = p.x;
         if  (p.y <  leftTop.y )
           leftTop.y = p.y;
         if  (p.y >  rightBottom.y )
           rightBottom.y = p.y;
      }
      else{
         this-> leftTop     = p;
         this-> rightBottom = p;
      }
    }
    else    
         fprintf (stderr, "\n expand(coor2 & p) : rectangle is not changed");
  }



