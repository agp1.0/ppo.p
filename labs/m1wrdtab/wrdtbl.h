#ifndef _WRDTBL_H 
#define _WRDTBL_H 
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#define    WSZ  50
typedef   unsigned int  uint;

struct  wrd {
   uint cnt;     // ������� ��������� �����
   char w[WSZ];  // ����� ��� �������� �����
};



// ������������� �������
extern 
int ini ( wrd * tbl, size_t tblSz, int debug = 0) ;     

// ������ ������� �� �����
extern 
int read ( wrd * tbl, size_t tblSz) ;     

// ����� ����� � �������
extern 
int indexOf ( const wrd * tbl, size_t tblSz
             , const char * word     // ����� ��� ������ � �������
            ); 

// ����� �������
extern 
void  print (const char * title,
              const wrd *tbl,  size_t tblSz);  

// ���������� �������
extern 
void  sort  (wrd *tbl,  size_t tblSz
             , int colNo     // ����������� �� ���� �������
             , int order     // ������� ����������
            );    

// ����� ���������� ����� ����������� a � b
extern 
void  swap1 (wrd *a, wrd *b);             

// ���������  ��������  �� ����� ��� ���������� ���������
// �������� ���������� ���������� strcmp
extern 
int   wrdcmp(const wrd *a, const wrd *b
         , int txtNmbr      // 0 ���������� �����, 1 ����������
         );      





extern 
int isInt(const char *str);

extern 
int isDouble(const char *str) ;


#endif