#include "wrdtbl.h"

#define SIZE 	500
#define SEP 	" \t"


int read ( wrd * tbl, size_t tblSz) {
 int  curSz = 0;            // ������ ����������� �������
 if (tbl){
        char *str = 0;
	char buf[SIZE]={0};
	int t=0;
	int lineno = 0;
	while(gets(buf)) {
	        lineno++;
		str = strtok(buf, SEP);
		while(str != NULL) {
		     t = indexOf ( tbl, curSz, str);  // ����� ����� � �������
                     if (t < 0){                    // �� �����
		       tbl[curSz].cnt=1;
		       strcpy(tbl[curSz].w, str);
 		       curSz++;
                       if (curSz >= tblSz)  {
                           printf( "table is overflow, word/line: '%s'/%d\n", str, lineno );
                           return curSz-1;
                       }
		     }
		     else                           // �����
                       tbl[t].cnt++;
		     str = strtok(NULL, SEP);
		}
	}
 }
 return curSz;
}


int indexOf ( const wrd * tbl, size_t tblSz, const char * str) {
  int i;
  for (i = 0; i< tblSz; i++) {
    if (strcmp(tbl[i].w, str) == 0)
       return i;
  }
  return -1;
}




void  sort  (wrd *tbl,  size_t tblSz, int colNo, int order){
 if (tbl){
    for (int i = 0; i < tblSz-1; i++)
      for (int j = tblSz; j > i; j-- ){
//         if (wrdcmp (&tbl[j-1], &tbl[i]))
         if (tbl[j-1].cnt > tbl[i].cnt)
            swap1(&tbl[j-1], &tbl[i]);
      }
 }
}


void  swap1 (wrd *a, wrd *b)             // ����� ���������� ����� ����������� a � b
{
  char ts [WSZ]= {0};
  if (a && b){
     int t = b->cnt;
     strcpy(ts, b-> w);

     b->cnt = a->cnt;
     strcpy(b->w, a->w);

     a->cnt = t;
     strcpy(a->w, ts);
  }
}




static char empty [WSZ]= {0};

int ini ( wrd * tbl, size_t tblSz, int debug) {

 if (tbl){
       for (int i = 0; i< tblSz; i++){
           tbl[i].cnt = 0;
           memcpy(tbl[i].w, empty, WSZ);
       }
       if (debug) {
          strcpy(tbl[0].w, "blabla"); 
          tbl[0].cnt  = -100; 
          strcpy(tbl[1].w, "nufoo"); 
          tbl[1].cnt  = -101; 
          return 3;
       }
 }
 return 0;
}
void  print (const char * title,
              const wrd *tbl,  size_t tblSz) {

 if (title && tbl ){
    printf ("table of word '%s'", title);
    for  (int i = 0; i < tblSz; i++){
       if (*(tbl[i].w) ) {     // ���� ���� ����� ������� �� �������
                               // ����� ��������� ����.
         printf ("\n rec/word/numbers:    %d : '%s' : %d"
         ,  i,  tbl[i].w, tbl[i].cnt   
         );
       }
    }
 }
}
