#include "wrdtbl.h"

//     ���������� �� ����������� �����������
void help(const char * nm){
  char appNm[100] = {0};
  char buf[200] = {0};
  strcpy(buf, nm);
  char *tmp = strrchr (buf, '\\'); 
  if (tmp)
     strcpy(appNm, tmp+1);
  else 
     strcpy(appNm,nm);
  
  printf("application to build table of word\n");
  printf("usage:\n");
  printf("%s  [-?] [-v] [-c NNN] [-desc | asc ]  -m MAX\nwhere\n", appNm);
  printf("-?	this page\n");
  printf("-d	to debug\n");
  printf("-v	to show additional info\n");
  printf("-c NNN 	column number to sort (1,2) // ignored in this version\n");
  printf("-desc | -asc	descending or ascending order // ignored in this version\n");
  printf("-m MAX 	max size of table\n");
  exit(1);      // ��� ������ ���������� �� 0
}


int main(int argc, char *argv[]) {
  int rc = 1;
  int maxTblSz  = -1;
  int order     = -1; // 1 �� �����������, 0 �� ��������, -1 �� �����
  int colNo     = -1; // ����� ������� ��� ����������
  int vFlag     = 0 ; // �������� ����� ������
  int dFlag     = 0 ; // �������
  int i = 1;
  time_t   st = time(0);
  for (;i< argc; i++){
     if(strcmp(argv[i], "-?") ==0 )
        help (argv[0]);
     else if (strcmpi(argv[i], "-v") ==0 )
        vFlag = 1;
     else if (strcmpi(argv[i], "-d") ==0 )
        dFlag = 1;
     else if (strcmpi(argv[i], "-desc") ==0 )
        order = 0;
     else if (strcmpi(argv[i], "-asc") ==0 )
        order = 1;
     else if (strcmpi(argv[i], "-c") ==0 ){
        if (++i < argc) {
           if (isInt (argv[i]))           // ���� ���������� � ��������� ����� ����� �������
               colNo = atoi(argv[i]);     //  ��� ����������
           else {
             printf("wrong param for -c\n");
             exit(2);
           }
        }
        else {
           printf("-c must to have parametr\n");
           exit(2);
        }
     }
     else if (strcmpi(argv[i], "-m") ==0 ) {
        if (++i < argc) {
           if (isInt (argv[i]) )   {      // ���� ���������� � ��������� �����  
               maxTblSz = atoi(argv[i]);  // ������������ ������ �������              
               if (maxTblSz > 0)
                   ;
               else {
                   printf("wrong param for -m\n");
                   exit(2);
               }
           }
           else {
             printf("wrong param for -m\n");
             exit(2);
           }
        }
        else {
           printf("-m must to have parametr\n");
           exit(2);
        }
     }
  }
  if (maxTblSz > 0) {
     rc = 0;
     if (vFlag)
        fprintf(stderr, " size of table/sort order/colNo/debug: %d/%d/%d/%d \n", maxTblSz, order, colNo, dFlag);

     wrd * tbl = 0;
                                     //  ������ ������ ��� �������
     tbl = (wrd *) malloc (maxTblSz * sizeof (wrd));
     if (tbl) {
        int sz = 
        ini(tbl, maxTblSz, dFlag ); // ��������� ������ ������.
        sz = read  (tbl, maxTblSz); // ������ ����� � ���������� �������
        if (colNo>=0)               // ���� ����� ����������
            sort (tbl, sz, colNo, order);
        print("title of table", tbl, sz);

        free(tbl); tbl = 0;         // ���������� ������
        rc = 0;
     }
     else {
       if (vFlag)
          fprintf(stderr, "no memory\n" );
     }

     if (vFlag)
        fprintf(stderr, "\n %d secs of work!\n",  time(0) - st);
  }
  else {
    if (vFlag)
       fprintf(stderr, "\nnothing to do!\n");
  }

  return rc;                    // ��� �������� ����� 0 ��� 1
}

