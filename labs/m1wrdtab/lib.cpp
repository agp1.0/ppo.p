#include "wrdtbl.h"


int isInt(const char *str)
{
	int digits = 0;
	int i = 0;

	if (str[0] == '\0')
		return 0;

	if ((str[0] == '+' || str[0] == '-') && str[1] != '\0')
		i++;

	while (str[i] != '\0')
	{
		if (!isdigit(str[i]))
			return 0;

		digits++;
		i++;

	}

	return digits>0?1:0;
}

int isDouble(const char *str)
{
	int i = 0;
	int digits = 0;
	char hasDot = 0;


	if ((str[0] == '+' || str[0] == '-') )
		i++;

	while (str[i] != '\0')
	{
		if (!isdigit(str[i]))
			if (str[i] == '.' && !hasDot)
				hasDot = 1;
			else
				return 0;
		else

		    digits ++;

		i++;
	}

	return digits>0?1:0;
}
