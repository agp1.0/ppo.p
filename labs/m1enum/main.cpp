#include <stdlib.h>
#include <stdio.h>

enum color {                      //����� color ����� ������������ 
   RED = 100,                     // ��� ��������  ����������
   GREEN,
   YELLOW
};

int main(int argc, char *argv[]) {
  int rc = 1;
  if (argc > 1) {
    color a = (color)atoi(argv[1]);     // ������� ���������� ���� color
    switch (a) {
      case(RED):     printf("Stop!\n");   rc =0;    break;
      case(GREEN):   printf("Go!\n");     rc =0;    break;
      case(YELLOW):  printf("Ready!\n");  rc =0;    break;
      default:       printf("Error. The numbers must be between 100 and 102.\n");
    }
  }                                                        	
  return rc;        // ��� �������� ����� 0 - ���� ��������� �����
                    //   ��� 1              - ���� ������� ��� ������� �����
}