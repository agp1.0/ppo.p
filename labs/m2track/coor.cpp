#include "lib.h"
#include "coor.h"


  int dFlag     = 0 ; // �������



  void coor2::ini (coor2 * this1, double x, double y, code_t wellFormed ){
     if (dFlag)
       fprintf (stderr, "\nini () is here");
     if (this1) {
        this1->x = x;
        this1->y = y;
        this1->wellFormed =  wellFormed;
     }
  }


  coor2::~coor2(){
     if (dFlag)
       fprintf (stderr, "\ndestructor coor2 () is here");
  }

  coor2::coor2(){
    if (dFlag)
       fprintf (stderr, "\ncoor2 () is here");
    ini(this, 0.0, 0.0, EMPTY);
  }


  int coor2::get ( const char * str1){
//    double x=0.0, y=0.0;
    int rc = -1;
    if (dFlag)
      fprintf (stderr, "\nget is here: '%s'", str1);
    if (str1){
      char * str = strRplc(0, str1);
      char * xs  = strtok  (str, SEPARATOR);
      rc=0;
      this -> wellFormed = EMPTY;
      if (xs)   {
        this -> wellFormed = WRONG;
        rc=1;
      //  if (dFlag)
      //    fprintf (stderr, "\nget is here: xs: '%s'", xs);
        if (isDouble(xs)){
           x = atof (xs);
           char * ys  = strtok  (0, SEPARATOR);
           rc = 2;
           if (ys)
              if (isDouble(ys)){
                 y = atof(ys);
  //               this ->x = x;
  //               this ->y = y;
                 this ->wellFormed = OK;
                 rc = 0;
                 if (dFlag)
                   fprintf (stderr, 
                          "\n get: ready: x/y: %f (%s)/%f(%s)"
                                 , x, xs, y, ys);
              }
        }
      }
      free(str); str=0;
    }
    return rc;
  }

  char * coor2::put (  char * str1){
     if (str1) {
       if (wellFormed!= OK)
         sprintf(str1, "#wrong data:%d", wellFormed);
       else {
         sprintf(str1, "%.6f %.6f", x, y);
       }

     }
     return str1;
  }

double * coor2::get(const  double * val) 
{  
   if (val) {
      x = val[0];
      y = val[1];
      wellFormed = OK;
   }
   return (double*)val;
}

double * coor2::put (double * val)
{
  if (val) {
    val[0] = x;
    val[1] = y;
  }
  return val;
}
