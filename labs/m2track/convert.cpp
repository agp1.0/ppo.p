#include "lib.h"
#include "coor.h"


#define LEN 3
static  double b[LEN];
static  char buf [200];


int  txt2bin(coor2 * rec, FILE * fl)
{
  int rc = 0;
  int  lineno = 0;
// char  *endLn=0;                           // ��������� �� ������ ����� ������
//  char   *str=0;
  char * endL=0;
  size_t sz = rec->size();

  while (gets(buf)){                        //  
    lineno++;
    endL= (char *)memchr(buf, '#', BUFSZ); // ����� � ������� ����������
    if (endL)
       *endL=0;
    if((rc = rec->get (buf))!=0)
       fprintf (stderr, "\ntxt2bin:  lineno %d, error: %d", lineno, rc);
    if (rec->wellFormed==OK)  {          
      rec->put(b);              // ����� ��� ����� � ������
      fwrite(b, 1, sz, fl);     // �� ������� b ������� sz ���� 
      if (dFlag)
        fprintf (stderr, "\ntxt2bin: lineno/wellFormed/string: %d/%d/'%s'"
           , lineno, rec->wellFormed, rec->put(buf));
                  
    }
                                     //  ��� ������� � ������������ ������
  }
  return 0;
} // stdin to file

int  bin2txt(coor2 * rec, FILE * fl)
{
   if (dFlag)
     fprintf (stderr, "\nbin2txt:  is here");
   size_t rLen=0;
   size_t sz = rec->size();
   while ((rLen = fread(b, 1, sz, fl))==sz){    // ������ ��� ����� sz ����
      rec->get(b);
      printf("\n%s", rec->put(buf));
   }
   if (rLen !=0)
      fprintf (stderr, "\nbin2txt: wrong rest of file:  %d"
           , rLen);
   return rLen;
} // file to stdout
