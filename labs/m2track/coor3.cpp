#include "lib.h"
#include "coor3.h"


extern   int dFlag; // �������





  void coor3::ini (coor2 * this1, double x, double y, double z, code_t wellFormed ){
     if (dFlag)
       fprintf (stderr, "\nini () is here");
     if (this1) {
        this1->x = x;
        this1->y = y;
        this1->y = z;
        this1->wellFormed =  wellFormed;
     }
  }


  coor3::~coor3(){
     if (dFlag)
       fprintf (stderr, "\ndestructor coor3 () is here");
  }

  coor3::coor3(){
    if (dFlag)
       fprintf (stderr, "\ncoor3 () is here");
    ini(this, 0.0, 0.0, 0.0, EMPTY);
  }


  int coor3::get ( const char * str1){
//    double x=0.0, y=0.0;
    int rc = -1;
    if (dFlag)
      fprintf (stderr, "\ncoor3::get is here: '%s'", str1);
    if (str1){
      char * str = strRplc(0, str1);
      char * xs  = strtok  (str, SEPARATOR);
      rc=0;
      this -> wellFormed = EMPTY;
      if (xs)   {
        this -> wellFormed = WRONG;
        rc=1;
        if (isDouble(xs)){
           x = atof (xs);
           char * ys  = strtok  (0, SEPARATOR);
           rc = 2;
           if (ys)
              if (isDouble(ys)){
                 y = atof(ys);
                 char * zs =  strtok  (0, SEPARATOR);
                 rc = 3;
                 if (zs)
                   if (isDouble(zs))  {
                      z =  atof(zs);
                      this ->wellFormed = OK;
                      rc = 0;
                      if (dFlag)
                        fprintf (stderr, 
                          "\n get: ready: x/y: %f (%s)/%f(%s)"
                                 , x, xs, y, ys);
                 }
              }
        }
      }
      free(str); str=0;
    }
    return rc;
  }

  char * coor3::put (  char * str1){
     if (str1) {
       if (wellFormed!= OK)
         sprintf(str1, "#wrong data:%d", wellFormed);
       else {
         sprintf(str1, "%.6f %.6f %.6f", x, y, z);
       }

     }
     return str1;
  }

double * coor3::get(const  double * val) 
{  
   if (val) {
      x = val[0];
      y = val[1];
      z = val[2];
      wellFormed = OK;
   }
   return (double*) val;
}

double * coor3::put (double * val)
{
  if (val) {
    val[0] = x;
    val[1] = y;
    val[2] = z;
  }
  return val;
}
