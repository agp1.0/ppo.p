#ifndef _COOR_H 
#define _COOR_H 



extern  int dFlag; 

enum code_t {
       WRONG
     ,	OK
     ,	EMPTY
};




class  coor2 {
 public:

  code_t  wellFormed;
  double x;          //  ��� �� �����
  double y;          //  ��� ����� ������

                     //   ����� ���������� �� �������
  virtual int      get(const  char * val);  // 1 ������ �������������������, 0 �����
                   //  ���������������� ���������� � ������
  virtual char  *  put (char * val);  // 1 ������ �������������������, 0 �����

                     //   ����� ���������� �� �������
  virtual double * get(const  double * val);  // 1 ������ �������������������, 0 �����
                     //  ���������������� ���������� � ������
  virtual double * put (double * val);  // 1 ������ �������������������, 0 �����
  virtual size_t   size() {return 2 * sizeof(double);}

  coor2();
  ~coor2 ();
  char * test () {return "I am coor2 object";}

  static void ini (coor2 * this1, double x, double y, code_t wellFormed );

};

extern int  txt2bin(coor2 * rec, FILE * fl); // stdin to file
extern int  bin2txt(coor2 * rec, FILE * fl); // file to stdout
  

#endif

