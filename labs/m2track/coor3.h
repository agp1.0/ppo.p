#ifndef _COOR3_H 
#define _COOR3_H 

#include "coor.h"


class  coor3:coor2 {
 public:
  double z;
                     //   ����� ���������� �� �������
  virtual int get(const  char * val);  // 1 ������ �������������������, 0 �����
                     //  ���������������� ���������� � ������
  virtual char  * put (char * val);  // 1 ������ �������������������, 0 �����

                     //   ����� ���������� �� �������
  virtual double * get(const  double * val);  // 1 ������ �������������������, 0 �����
                     //  ���������������� ���������� � ������
  virtual double * put (double * val);  // 1 ������ �������������������, 0 �����
  virtual size_t   size() {return 3 * sizeof(double);}

  void ini (coor2 * this1, double x, double y, double z, code_t wellFormed );

    char * test () {return "I am coor3 object";}

  coor3();
  ~coor3 ();
  
};


#endif
