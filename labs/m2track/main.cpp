
#include <time.h>

#include "lib.h"
#include "coor3.h"

                

//     ���������� �� ����������� �����������
void help(const char * nm){
  char appNm[100] = {0};
  char buf[200] = {0};
  strcpy(buf, nm);
  char *tmp = strrchr (buf, '\\'); 
  if (tmp)
     strcpy(appNm, tmp+1);
  else 
     strcpy(appNm,nm);
  
  printf("application to convert a track from/to text/binary file\n");
  printf("usage:\n");
  printf("%s  [-?] [-v] [-d] {-b2t | -t2b} -f name \nwhere\n", appNm);
  printf("-?	  this page\n");
  printf("-d	  to debug\n");
  printf("-v	  to show additional info\n");
  printf("-3	  x,y,z, opposit: x,y\n");
  printf("-t2b    to convert stdin to binary file name \n");
  printf("-b2t    to convert binary file name  to stdout\n");
  printf("-f name name is name of binary file\n");

  exit(1);      // ��� ������ ���������� �� 0
}


int main(int argc, char *argv[]) {

  int rc = 1;
  int direction     = 0;     //  -1 ����� � ��������, 1 - �������� � ���������
  int vFlag         = 0;     // �������� ����� ������
  int coor3Flag     = 0;     // 3 ����������
  char  flNm[PATH_MAX+1]={0};// ��� �����
  int i = 1;
  time_t   st = time(0);
  for (;i< argc; i++){
     if(keycmp(argv[i], "?") )
        help (argv[0]);
     else if (keycmp(argv[i], "h") )
        help (argv[0]);
     else if (keycmp(argv[i], "v") )
        vFlag = 1;
     else if (keycmp(argv[i], "d") )
        dFlag = 1;
     else if (keycmp(argv[i], "3") )
        coor3Flag = 1;
     else if (keycmp(argv[i], "t2b") )
        direction = -1;
     else if (keycmp(argv[i], "b2t") )
        direction =  1;
     else if (keycmp(argv[i], "f") ) {
        if (i+1 < argc)
           strcpy(flNm, argv[++i]);
        else  {
           fprintf(stderr, "no name of file!");
           exit(rc);
        }
     }

  }

  if (strlen(flNm)<1 || direction == 0) {
           fprintf(stderr, "nothing to do!");
           exit(rc);
  }

  rc = 0;

  if (vFlag)
     fprintf(stderr, " file/direction/debug: '%s;/%d/%d \n"
              , flNm, direction,  dFlag);

  coor2 *rec=0;               
  if (coor3Flag) {
    coor3 * rec3 =  new coor3();
    rec = (coor2*) rec3;
    if (vFlag)
      fprintf(stderr, "\n object function3: '%s'/'%s'  ", rec3->test(), rec->test());
  }
  else 
    rec = new coor2();



  FILE *bf = 0;

  if(  direction < 0) {
     bf = fopen(flNm, "wb");
     if (bf) 
       rc = txt2bin(rec, bf);
  }
  else {
     bf = fopen(flNm, "rb");
     if (bf) 
       rc = bin2txt(rec, bf);
  }
  if (bf) {
     fclose (bf); bf=0;
  }
  else {
    perror(flNm);
  }
  if (vFlag)
     fprintf(stderr, "\n size of record is %d!\n ",  rec->size());

  delete rec; rec =0;
  if (vFlag)
     fprintf(stderr, "\n %d secs of work!\n ",  time(0) - st);

  return rc;                    // ��� �������� ����� 0 ��� 1
}

