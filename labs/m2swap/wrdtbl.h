#ifndef _WRDTBL_H 
#define _WRDTBL_H 
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include "lib.h"

//#define    WSZ  50
typedef   unsigned int  uint;

struct  wrd {
   uint cnt;     // ������� ��������� �����
   char *w;  // ����� ��� �������� �����
   wrd * tail;   // ������� (�����) ������

   wrd (){
     cnt = 0;
     w = 0;
     tail = 0;
   }


   wrd (const wrd& b) {
//      fprintf(stderr, "\n *** ctor wrd is here \n");
     w    = strRplc(0, b.w);
     cnt  = b.cnt;
     tail = 0;
   }
    wrd & operator=(const wrd& b){ // �������� ����������
     // printf("\n *** = is here \n");
      w     = strRplc(w, b.w);
      cnt   = b.cnt;
      tail  = 0;//b.tail;
     return *this;
    }     

   ~wrd ( ){
           cnt  = 0;
           if(w != 0) {free(w); w=0;}
           tail = 0;
   } 

};


struct tbl{
   wrd  * t;
   tbl (){
//      fprintf(stderr, "\n ctor tbl is here \n");
      t = 0;
   }

   ~tbl (){
    //delete t;
//      fprintf(stderr, "\n destructor tbl is here \n");
    wrd *temp  = t;
    while  (temp){
        t = t->tail;
        delete temp;
        temp = t;
    } 
   }

   int             // 1: �������� �������, 0:  ���
   add (const char * word);

};


// ���������� �������  � �������
extern 
void  sort  (wrd *tbl
             , int sz 
             , int colNo     // ����������� �� ���� �������
             , int order     // ������� ����������
            );    
// ���������� �������  � ������
extern 
void  sort1  (wrd *tbl
             , int sz 
             , int colNo     // ����������� �� ���� �������
             , int order     // ������� ����������
            );    

extern 
void  print (const char * title,
              const tbl  *table) ;


extern 
wrd * elemOf ( const wrd * tbl,  const char * str) ;

extern 
int read ( tbl * tbl) ;


#endif

//  printf ("",);
