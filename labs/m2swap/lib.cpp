#ifdef _DEBUG

#include <crtdbg.h>
//#define _CRTDBG_MAP_ALLOC
#endif
#include "lib.h"


void checkMemory(){
#ifdef _DEBUG
//  ��������� ������� �������� �� �������� ������
     _CrtSetDbgFlag(
                 _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG)
              | _CRTDBG_CHECK_ALWAYS_DF 
              | _CRTDBG_LEAK_CHECK_DF
        );

//  ��� ���� ���������
//  �������� � ���� ������������ ������

     _CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);
     _CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDERR);
     _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDERR);
#endif

}




int keycmp( const char * arg, const char * key){
  int rc = 0;
  if (arg && key)  
     if (*arg == '-' || *arg == '/' ) 
       if (strcmpi (arg+1, key) == 0)
         rc = 1;
  return rc;
}



char * strRplc(char * oldstr, const char * str){
   char * rc = 0;
   if (oldstr){        // ���������� ������ ������
   	  free(oldstr);
   	  oldstr = 0;
   }
   if (str) {          // �������� ����� 
      size_t l = strlen(str);
      rc  = (char *) malloc (l+1);
      strcpy(rc, str);
   }
//     fprintf (stderr,           "\nstrRplc %d/'%s'", rc, rc );

   return rc;
}

                
int isInt(const char *str)
{
	int digits = 0;
	int i = 0;

	if (str[0] == '\0')
		return 0;

	if ((str[0] == '+' || str[0] == '-') && str[1] != '\0')
		i++;

	while (str[i] != '\0')
	{
		if (!isdigit(str[i]))
			return 0;

		digits++;
		i++;

	}

	return digits>0?1:0;
}

int isDouble(const char *str)
{
	int i = 0;
	int digits = 0;
	char hasDot = 0;


	if ((str[0] == '+' || str[0] == '-') )
		i++;

	while (str[i] != '\0')
	{
		if (!isdigit(str[i]))
			if (str[i] == '.' && !hasDot)
				hasDot = 1;
			else
				return 0;
		else

		    digits ++;

		i++;
	}

	return digits>0?1:0;
}
