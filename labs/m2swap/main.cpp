#include "wrdtbl.h"
#include <algorithm>    // std::swap


//     ���������� �� ����������� �����������
void help(const char * nm){
  char appNm[100] = {0};
  char buf[200] = {0};
  strcpy(buf, nm);
  char *tmp = strrchr (buf, '\\'); 
  if (tmp)
     strcpy(appNm, tmp+1);
  else 
     strcpy(appNm,nm);
  
  printf("application to build table of word\n");
  printf("usage:\n");
  printf("%s  [-?] [-v] [-c NNN] [-desc | asc ]  -m MAX\nwhere\n", appNm);
  printf("-?	this page\n");
  printf("-d	to debug\n");
  printf("-v	to show additional info\n");
  printf("-c NNN 	column number to sort (1,2) // ignored in this version\n");
  printf("-desc | -asc	descending or ascending order // ignored in this version\n");
  exit(1);      // ��� ������ ���������� �� 0
}

void swap2 ( wrd& a, wrd& b )
{
  wrd c(a); //a=b; b=c;
}



int main(int argc, char *argv[]) {
  checkMemory();
  int rc = 1;
  int order     = -1; // 1 �� �����������, 0 �� ��������, -1 �� �����
  int colNo     = -1; // ����� ������� ��� ����������
  int vFlag     = 0 ; // �������� ����� ������
  int dFlag     = 0 ; // �������
  int i = 1;
  time_t   st = time(0);
  for (;i< argc; i++){
     if(strcmp(argv[i], "-?") ==0 )
        help (argv[0]);
     else if (strcmpi(argv[i], "-v") ==0 )
        vFlag = 1;
     else if (strcmpi(argv[i], "-d") ==0 )
        dFlag = 1;
     else if (strcmpi(argv[i], "-desc") ==0 )
        order = 0;
     else if (strcmpi(argv[i], "-asc") ==0 )
        order = 1;
     else if (strcmpi(argv[i], "-c") ==0 ){
        if (++i < argc) {
           if (isInt (argv[i]))           // ���� ���������� � ��������� ����� ����� �������
               colNo = atoi(argv[i]);     //  ��� ����������
           else {
             printf("wrong param for -c\n");
             exit(2);
           }
        }
        else {
           printf("-c must to have parametr\n");
           exit(2);
        }
     }
  }
  rc = 0;
  if (vFlag)
     fprintf(stderr, " sort order/colNo/debug: %d/%d/%d \n",  order, colNo, dFlag);

  tbl * t = 0; 
  t = new tbl();
  if (dFlag) {
    wrd a;

    a.w=strRplc(0, "kuku22");
    wrd b;
    b.w=strRplc(0, "kuku");


//    std::swap(a,b);
//   extern void  swap1 (wrd &a, wrd &b);

    swap2(a,b);

/*
    t->add("foo");
    t->add("test");
    t->add("test");
    wrd * a = t->t;
    wrd *b  = t->t->tail;
    std::swap (*a, *b);

    */
    delete t;  
    return 0;
  }
  size_t sz = read(t);
  if (colNo>=0)               // ���� ����� ����������
    sort1 (t->t, sz,  colNo, order);


  print ("table", t);
  delete t;
  t = 0;
 if (vFlag)
     fprintf(stderr, "\n %d secs of work! size of t is %d\n",  (int)(time(0) - st), sz);


  return rc;                    // ��� �������� ����� 0 ��� 1
}

