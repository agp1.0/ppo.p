#ifndef _LIST_H 
#define _LIST_H 
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "lib.h"

typedef unsigned int uint;



struct  line{
       static uint   total; // ����������� ���������� ��� �������� ���-��
                            // ��������� ��������� ������.
       char *        text; 
       line *        next;
};




extern
void addStr  ( const char *str); //  ���������� ������� � ������
extern
void freeMem  ( );              //  ������������� ������
extern
void priMem  ();                // ����� ����������� ���������� �����

 
// ������� ������� ������ -- ������ ������������
 
extern
line *mkLine  (const char * str);  

 // �������� ��������  -- ������ �����������
extern
line * delLine  (line * list);      




#endif