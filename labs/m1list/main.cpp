#ifdef _DEBUG
#include <crtdbg.h>
//#define _CRTDBG_MAP_ALLOC
#endif

#include "list.h"
#include "dtTmTo.h"

#define  BUFSZ 1000


//     ���������� �� ����������� �����������
void help(const char * nm){
  char appNm[100] = {0};
  char buf[200] = {0};
  strcpy(buf, nm);
  char *tmp = strrchr (buf, '\\'); 
  if (tmp)
     strcpy(appNm, tmp+1);
  else 
     strcpy(appNm,nm);
  
  printf("application to build table of words\n");
  printf("usage:\n");
  printf("%s  [-?] [-v]\nwhere\n", appNm);
  printf("-?	this page\n");
  printf("-d	to debug\n");
  printf("-v	to show additional info\n");
  exit(1);      // ��� ������ ���������� �� 0
}



void main(int argc, char *argv[]) {

#ifdef _DEBUG
//  ��������� ������� �������� �� �������� ������
     _CrtSetDbgFlag(
                 _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG)
              | _CRTDBG_CHECK_ALWAYS_DF 
              | _CRTDBG_LEAK_CHECK_DF
        );

//  ��� ���� ���������
//  �������� � ���� ������������ ������

     _CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);
     _CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDERR);
     _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
     _CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDERR);
#endif

 //
 //   ��� ��� ��� ���������� �������
 //
  int vFlag     = 0 ; // �������� ����� ������
  int dFlag     = 0 ; // �������
  int i = 1;

  for (;i< argc; i++){
     if(keycmp(argv[i], "?") )
        help (argv[0]);
     else if (keycmp(argv[i], "v") )
        vFlag = 1;
     else if (keycmp(argv[i], "d") )
        dFlag = 1;
  }
  if (dFlag)
     malloc(10);                             // ��������� ��������� ���������� �� �������

  time_t st =  time (0);                      // ������� ����������� ����� � �������� � 1970 ����

  char  buf [BUFSZ]= {0};
  uint lineno = 0;
  char  *newLn=0;                             // ��������� �� ������ ����� ������
  while (fgets(buf, BUFSZ, stdin)){          //  fgets ��������� ������� ����� ������
//  while (gets(buf)){
    newLn= (char *)memchr(buf, '\n', BUFSZ);         // ��� ���� ����� � �������.
    if (newLn)
       *newLn=0;
    lineno++;
    addStr(buf);
  }
  if (ferror(stdin)) {                       // ��������� ������ ��� ����� �����
        perror ("error while reading stdin");
        exit (1);
  }


  if (vFlag)
      fprintf (stderr, "\n%s: I have red %d lines"
                                         , dttmtoa(time(0)), lineno);
  priMem();
  freeMem();

  if (vFlag){
      fprintf (stderr, "\n%s: my work time  is %d secs,"
                                         , dttmtoa(time(0)), (int)difftime(time(0), st));
  
  }   
}


  //   fprintf (stderr, "--->'%s'  '%s'", arg, key);

 