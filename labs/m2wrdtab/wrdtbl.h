#ifndef _WRDTBL_H 
#define _WRDTBL_H 
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include "lib.h"

#define    WSZ  50
typedef   unsigned int  uint;

struct  wrd {
   uint cnt;     // ������� ��������� �����
   char w[WSZ];  // ����� ��� �������� �����
   wrd * tail;   // ������� (�����) ������

   ~wrd ( ){
           cnt  = 0;
           w[0] = 0;
           tail = 0;
   } 
};

struct tbl{
   wrd  * t;
   tbl (){
      t = 0;
   }
   ~tbl (){
    //delete t;
    wrd *temp  = t;
    while  (temp){
        t = t->tail;
        delete temp;
        temp = t;
    } 
   }
   int             // 1: �������� �������, 0:  ���
   add (const char * word);
};


// ���������� �������
extern 
void  sort  (wrd *tbl
             , int sz 
             , int colNo     // ����������� �� ���� �������
             , int order     // ������� ����������
            );    
// ���������� �������
extern 
void  sort1  (wrd *tbl
             , int sz 
             , int colNo     // ����������� �� ���� �������
             , int order     // ������� ����������
            );    

extern 
void  print (const char * title,
              const tbl  *table) ;


extern 
wrd * elemOf ( const wrd * tbl,  const char * str) ;

extern 
int read ( tbl * tbl) ;


#endif

//  printf ("",);
