#include "wrdtbl.h"

#define SIZE 	500
#define SEP 	" \t"


void  
print (const char * title,
              const tbl  *table) {

 if (title  ){
    printf ("\ntable of word '%s'", title);
    int i = 0;

    if (table)  {
       wrd *t = table->t;
       while (t) {
            printf ("\n rec/word/numbers:    %d : '%s' : %d"
              ,  i++,  t->w, t->cnt   
            );
            t= t->tail;
       }
    }
 }
}

wrd * elemOf ( const wrd * tbl1,  const char * str) {
  wrd * tbl = (wrd *) tbl1;
  if (str) {
     int i = 0;
     while (tbl) {
       if (strcmp(tbl->w, str) == 0) {
            return tbl;
       }
       tbl= tbl->tail;
     }
  }
  return 0;
}




int  tbl:: add (const char * word){     // 1: �������� �������, 0:  ���
   int rc = 0;
   if (word) {
      wrd * tmp = elemOf (t, word); 
      if (tmp) 
          tmp ->cnt ++;
      else  {
          tmp = (wrd*) malloc(sizeof (wrd));
          strcpy(tmp-> w, word);
          tmp->cnt = 1;
          tmp->tail = t;
          t=tmp;
          rc = 1;

      }
   }
   return rc;
}

int read ( tbl * tbl) {
 int  curSz = 0;            // ������ ����������� �������
 if (tbl ){
    char *str = 0;
	char buf[SIZE]={0};
	int t=0;
	int lineno = 0;
	while(gets(buf)) {
	        lineno++;
		str = strtok(buf, SEP);
		while(str != NULL) {
		     if(tbl->add(str))
		       curSz++;
		     str = strtok(NULL, SEP);
		}
	}
 }
//printf ("\ncurSz2 ---> %d", curSz);
 return curSz;
}

void  swap1 (wrd &a, wrd &b)             // ����� ���������� ����� ����������� a � b
{
  char ts [WSZ]= {0};
     int t = b.cnt;
     strcpy(ts, b.w);

     b.cnt = a.cnt;
     strcpy(b.w, a.w);

     a.cnt = t;
     strcpy(a.w, ts);
}

/*
void  print1 (const char * title,
               wrd **tbl,  size_t tblSz) {

 if (title && tbl ){
    printf ("\narray of word '%s'", title);
    for  (int i = 0; i < tblSz; i++){
       if (*(tbl[i]->w) ) {     // ���� ���� ����� ������� �� �������
                               // ����� ��������� ����.
         printf ("\n rec/word/numbers:    %d : '%s' : %d : %u"
         ,  i,  tbl[i]->w
             , tbl[i]->cnt
             ,  tbl[i]->tail
         );
       }
    }
 }
} */



void  
sort  (wrd **tbl,  size_t tblSz, int colNo, int order){
 if (tbl){
    for (int i = 0; i < tblSz-1; i++)
      for (int j = tblSz; j > i; j-- ){
//         if (wrdcmp (&tbl[j-1], &tbl[i]))
         if (tbl[j-1]->cnt > tbl[i]->cnt)
            swap1(*tbl[j-1], *tbl[i]);
      }
 }
}



void  sort1  (wrd *tbl
             , int sz
             , int colNo     // ����������� �� ���� �������
             , int order     // ������� ����������
            ){
   if (tbl && sz > 0) {
   	 wrd ** t2 = (wrd**)malloc (sizeof(wrd *) * sz );             
   	 if (t2) {
   	    wrd * tmp = tbl;
   	    int i = 0;
   	    for (; i< sz && tmp; i++){  //  ������� ������
   	      t2[i] = tmp;
   	      tmp = tmp->tail;
   	    }
   	    sort(t2, sz, colNo, order);
   	    for (int j =0; j< i-1 ; j++) //  ������� ������
   	       t2[j]->tail= t2[j+1];
   	    free (t2); t2=0;
   	 }
   }
}    

                                 //  printf ("after array %d/%d", i, sz);
                        ///   	    print1("1111",  t2, sz);


