//#include"Header.h"
#include <iostream>
#include <ctype.h>
#include<stdio.h>
#include<string.h>
//#include <crtdbg.h>
//#define ERROR 1

class word {
public:  
  char * w;
  int    cnt;
  word (const char * w){
    if (w) {
       this->w=(char *)malloc(strlen(w)+1);
       strcpy(this->w, w);
       cnt=1;
    }
    else {
       w = 0;
       cnt=0;
    }
  }
  ~word(){
      printf("\n *** destructor \n");
    if(w) {
       free(w);
       w=0;
    }
  }

/*       конструктор копирования
  word (const word& b) {
    printf("\n ***constructor is here\n");
    if (b.w) {
      w= (char *)malloc(strlen(b.w)+1);
      strcpy(this->w, b.w);
      cnt = b.cnt;
    }
    else {
      w=0;
      cnt = 0;
    }
  } */

#ifndef ERROR
  word & operator=(const word& b){ // операция присвоения
    printf("\n *** = is here \n");
    if(w) {
       printf("\n *** free old \n");
       free(w);
       w=0;
       cnt = 0;
    }
    else {
       this->w=0;
       cnt = 0;
    }
    if (b.w) {
      printf("\n *** malloc new \n");

      w= (char *)malloc(strlen(b.w)+1);
      strcpy(this->w, b.w);
      cnt = b.cnt;
    }
    return *this;
  }
#endif
};







int main()
{
_CrtSetDbgFlag(
_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG)
| _CRTDBG_CHECK_ALWAYS_DF
| _CRTDBG_LEAK_CHECK_DF
);
_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);
_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
_CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDERR);
_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
_CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDERR);

   word a("aaa1"), b("bbb");
   a=b;            //  использование операции присвоения
   printf("a/b: '%s'/'%s'",a.w, b.w);
}


