#include "dtTmTo.h"

static char  buf[64];


char * dttoa( time_t dtTm, char *b ){
  struct tm * dttm = localtime(&dtTm);
  return dtToA(  dttm, b ); 
}
char * tmtoa( time_t dtTm, char *b ){
  struct tm * dttm = localtime(&dtTm);
  return tmToA(  dttm, b ); 
}
char * dttmtoa( time_t dtTm, char *b ){
  struct tm * dttm = localtime(&dtTm);
  return dtTmToA(  dttm, b ); 
}




char * dtToA( const struct tm  * dtTm, char *b ){
  if (dtTm){
    if (b==0)
      b=buf;
    sprintf(b, "%04d/%02d/%02d", 1900+dtTm->tm_year, 1+dtTm->tm_mon, dtTm->tm_mday);
    return b;
  }
  return 0;                                                                       
}

char * tmToA( const struct tm * dtTm, char *b){
  if (dtTm){
    if (b==0)
      b=buf;
    sprintf(b, "%02d:%02d:%02d", dtTm->tm_hour, dtTm->tm_min, dtTm->tm_sec);
    return b;
  }
  return 0;                                                                       
}

char * dtTmToA( const struct tm * dtTm, char *b){
  if (dtTm){
    char dt1[32]={0};
    char tm1[32]={0};
    if (b==0)
      b = buf;
      sprintf (b, "%s %s", dtToA(dtTm, dt1), tmToA(dtTm, tm1));
      return b;
  }
  return 0;                                                                       
}                                                        