#include <stdio.h>
#include <stdlib.h>
#include <process.h>

#define  APP "a.exe"
#define  N   10

int main(int argc, char * argv[]){
 int rc = 0;
 long pid = _getpid();
 long ppid = 0;//_getppid();
 printf ("started pid/argc: %d/%d\n", pid, argc);

 if (argc <= 1) {
   printf ("no any param\n");
 }
 else {
   rc = atoi (argv[1]);
   char buf[10];
   char * _argv[3];
   _argv[0] = APP;
   _argv[1] = buf;
   _argv[2] = 0;
   rc ++;
   if (rc < N)  {
     sprintf(buf, "%d", rc);
     printf ("execv pid/argc/rc: %d/%d/%d\n", pid, argc, rc);
     system("pause");
     _execv(APP, _argv);
   }
   else {
     printf ("no new execv\n");
   }

 }
 printf ("finisheded pid/argc/rc: %d/%d/%d\n", pid, argc, rc);

 exit (rc);
}