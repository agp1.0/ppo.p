//#include"Header.h"
#include <iostream>
#include <ctype.h>
#include<stdio.h>
#include<string.h>
using namespace std;
//#include <crtdbg.h>

class word {
public:  
  char * w;
  int    cnt;

  word (const char * w){
    if (w) {
       this->w=(char *)malloc(strlen(w)+1);
       strcpy(this->w, w);
       cnt=1;
    }
    else {
       this->w = 0;
       cnt=0;
    }
  } 
  ~word(){
      printf("\n *** destructor");
    if(w) {
       printf(":'%s'", w);
       free(w);
       w=0;
    }
  } 

  friend ostream& operator<< (ostream &out, const word& o){
     if (o.w)
       out<<"word/count: '"<<o.w<< "'/"<<o.cnt;
     else 
       out<<"word/count: ''/"<<o.cnt;
     return out;
  }
};

int main()
{
   word a("aaa1"), b(0);
   cout<< a<<"\n"<<b;
   return 0;
}

